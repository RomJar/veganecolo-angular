import { Injectable } from '@angular/core';
import { User } from '../components/users/user';

@Injectable()


export class AccountService {

  private me: User = null;

  constructor() { }

  public connect(user: User) {
    this.me = user;
  }

  public get() {
    return this.me;
  }

}