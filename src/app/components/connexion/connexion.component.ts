import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl } from '@angular/forms';
import { FormBuilder } from '@angular/forms';
import { Validators } from '@angular/forms';
import { FormArray } from '@angular/forms';
import { User } from '../users/User';


@Component({
  selector: 'app-connexion',
  templateUrl: './connexion.component.html',
  styleUrls: ['./connexion.component.css']
})
export class ConnexionComponent implements OnInit {

  profileForm = this.fb.group({
    pseudo: ['', Validators.required],
    mdp: ['', Validators.required],
    age: ['', Validators.required, Validators.pattern("^[0-9]{1,3}$") ],
    departement: ['', Validators.required]
  });

  createAccount() {

    let newUser: User = new User() ;

    newUser.pseudo = this.profileForm.value.pseudo;
    newUser.password = this.profileForm.value.password;

    console.log(this.profileForm.value.pseudo);

  }

  constructor(private fb: FormBuilder) { }

  ngOnInit() {
  }

}
