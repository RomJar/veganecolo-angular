import { Component, OnInit } from '@angular/core';
import { User } from 'app/components/users/User';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { AccountService } from 'app/services/account.services';
import { HtmlParser } from '@angular/compiler';

@Component({
  selector: 'app-connexion2',
  templateUrl: './connexion2.component.html',
  styleUrls: ['./connexion2.component.css']
})
export class Connexion2Component implements OnInit {

  public users: User[] = [
    new User(1, "Nitro", "R31v1t3m"),
    new User(2, "Pierrick", "4ng0t"),
    new User(3, "Léa", "B0uv13r"),
    new User(4, "Gwenola", "L3_G41|u63c"),
    new User(5, "Kevin", "V4nd3rc4m3r3"),
    new User(6, "Thibault", "Fr4nch3squ1n"),
    new User(7, "Charlène", "Dub0c"),
    new User(8, "Clément", "L4ur0nt"),
    new User(9, "Alexandre", "Gr0nd1n"),
    new User(10, "Andréa", "F0nt3n34u"),
    new User(11, "Nicolas", "R31v1t3m"),
    new User(12, "Cyrille", "Fr4nc01s")
  ];

  constructor ( private accountService: AccountService) {

  }

  login(){
    let user = (<HTMLInputElement>document.querySelector("#pseudo")).value;
    let password = (<HTMLInputElement>document.querySelector("#mdp")).value;

    for (const x of this.users) {
      if (user == x.pseudo) {
        if (password == x.password) {
          alert("Tu es connecté, va sur la page d'accueil.")
        } else { alert("Pas le bon mot de passe")}; break;
      } else { alert("Pas le bon pseudo ")}; break;
    }
    
    //this.accountService.connect(newUser);
    
  }

  ngOnInit() {
}

}


