/* tslint:disable:no-unused-variable */
import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { By } from '@angular/platform-browser';
import { DebugElement } from '@angular/core';

import { Connexion2Component } from './connexion2.component';

describe('Connexion2Component', () => {
  let component: Connexion2Component;
  let fixture: ComponentFixture<Connexion2Component>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ Connexion2Component ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(Connexion2Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
