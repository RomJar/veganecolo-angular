import { Component, OnInit } from '@angular/core';
import { User } from './User';

@Component({
  selector: 'app-users',
  templateUrl: './users.component.html',
  styleUrls: ['./users.component.css']
})
export class UsersComponent implements OnInit {

  public users: User[] = [
    new User(1, "Nitro", "R31v1t3m"),
    new User(2, "Pierrick", "4ng0t"),
    new User(3, "Léa", "B0uv13r"),
    new User(4, "Gwenola", "L3_G41|u63c"),
    new User(5, "Kevin", "V4nd3rc4m3r3"),
    new User(6, "Thibault", "Fr4nch3squ1n"),
    new User(7, "Charlène", "Dub0c"),
    new User(8, "Clément", "L4ur0nt"),
    new User(9, "Alexandre", "Gr0nd1n"),
    new User(10, "Andréa", "F0nt3n34u"),
    new User(11, "Nicolas", "R31v1t3m"),
    new User(12, "Cyrille", "Fr4nc01s")
  ];

  user: User = this.users[0];

  constructor() { }

  ngOnInit() {
  }

}
