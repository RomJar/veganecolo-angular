/* tslint:disable:no-unused-variable */
import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { By } from '@angular/platform-browser';
import { DebugElement } from '@angular/core';

import { PageEcoloComponent } from './pageEcolo.component';

describe('PageEcoloComponent', () => {
  let component: PageEcoloComponent;
  let fixture: ComponentFixture<PageEcoloComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PageEcoloComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PageEcoloComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
