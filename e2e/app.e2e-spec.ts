import { VeganecoloPage } from './app.po';

describe('veganecolo App', function() {
  let page: VeganecoloPage;

  beforeEach(() => {
    page = new VeganecoloPage();
  });

  it('should display message saying app works', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('app works!');
  });
});
